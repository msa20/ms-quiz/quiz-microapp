import {
  QuizAnswerStateKeeper,
  QuizAnswersStateKeeper,
  QuizAnswerSheetStateKeeper,
  QuizAnswerSheetsStateKeeper,
  QuizSheetStateKeeper,
  QuizSheetsStateKeeper,
  QuizStateKeeper,
  QuizzesStateKeeper,
} from './keeper';


export const store = {
  quizAnswerStateKeeper: QuizAnswerStateKeeper.instance,
  quizAnswersStateKeeper: QuizAnswersStateKeeper.instance,
  quizAnswerSheetStateKeeper: QuizAnswerSheetStateKeeper.instance,
  quizAnswerSheetsStateKeeper: QuizAnswerSheetsStateKeeper.instance,
  quizSheetStateKeeper: QuizSheetStateKeeper.instance,
  quizSheetsStateKeeper: QuizSheetsStateKeeper.instance,
  quizStateKeeper: QuizStateKeeper.instance,
  quizzesStateKeeper: QuizzesStateKeeper.instance,
};

export {
  QuizAnswerStateKeeper,
  QuizAnswersStateKeeper,
  QuizAnswerSheetStateKeeper,
  QuizAnswerSheetsStateKeeper,
  QuizSheetStateKeeper,
  QuizSheetsStateKeeper,
  QuizStateKeeper,
  QuizzesStateKeeper,
};
