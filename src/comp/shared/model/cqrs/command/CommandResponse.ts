
import FailureMessage from '../../FailureMessage';


class CommandResponse {
  //
  entityIds: string[] = [];
  failureMessageName: string = 'failureMessage';

  getEntityId(): string {
    //
    const entityId = this.entityIds[0];

    if (!entityId) {
      throw new Error('CommandResponse -> entityId is null');
    }
    return entityId;
  }

  get success(): boolean {
    //
    return !this[this.failureMessageName];
  }

  get failed(): boolean {
    //
    return !!this[this.failureMessageName];
  }

  getFailureMessage(): FailureMessage {
    //
    return this[this.failureMessageName];
  }

  setFailureMessage(failureMessage: FailureMessage): void {
    //
    this[this.failureMessageName] = failureMessage;
  }

  [key: string]: any;
}

export default CommandResponse;
