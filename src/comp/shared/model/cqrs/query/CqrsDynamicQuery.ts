

import QueryParam from './QueryParam';
import Connector from './Connector';
import QueryParams from './QueryParams';
import { Offset } from '~/comp/shared';


class CqrsDynamicQuery<T extends object | object[]> {
  //
  queryParams: QueryParams | null;
  offset: Offset;

  constructor(queryParam?: QueryParam) {
    //
    this.queryParams = queryParam ? QueryParams.fromParam(queryParam) : null;
    this.offset = Offset.newAscending(0, 10, 'id');
    this.offset.totalCountRequested = false;
  }

  static oneParam<T extends object | object[]>(queryParam: QueryParam): CqrsDynamicQuery<T> {
    //
    queryParam.connector = Connector.End;

    return new CqrsDynamicQuery<T>(queryParam);
  }

  static multiParams<T extends object | object[]>(...queryParams: QueryParam[]): CqrsDynamicQuery<T> {
    //
    if (queryParams.length === 0) {
      throw new Error('CqrsDynamicQuery.multiParams -> queryParams, Length of queryParams must be more 1');
    }

    const newQueryParams = queryParams.map((queryParam, index: number) => {
      const last = index === queryParams.length - 1;

      if (last && queryParam.connector !== Connector.End) {
        queryParam.connector = Connector.End;
      }
      else if (!last && queryParam.connector === Connector.End) {
        throw new Error('CqrsDynamicQuery.multiParams -> ConnectorType must not be End in the middle of queryParams');
      }

      return queryParam;
    });

    const query = new CqrsDynamicQuery<T>();

    query.queryParams = QueryParams.fromParams(newQueryParams);

    return query;
  }
}

export default CqrsDynamicQuery;
