
import { Offset } from '~/comp/shared';


class CqrsBaseQuery<T extends object | object[]> {
  //
  id?: string;
  offset?: Offset;
}

export default CqrsBaseQuery;

