
enum Operator {
  Equal = 'Equal',
  NotEqual = 'Equal',
  GreaterThan = 'GreaterThan',
  GreaterThanOrEqual = 'GreaterThanOrEqual',
  LessThan = 'LessThan',
  LessThanOrEqual = 'LessThanOrEqual',
  In = 'In',
  NotIn = 'NotIn',
  Like = 'Like',
  NotLike = 'NotLike',
}

export default Operator;
