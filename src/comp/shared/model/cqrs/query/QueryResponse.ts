
import { Offset } from '../../offset';
import FailureMessage from '../../FailureMessage';


class QueryResponse<T extends object | object[]> {
  //
  offset?: Offset = undefined;
  failureMessageName: string = 'failureMessage';

  get queryResult(): T | T[] | null {
    //
    return this.getResponse();
  }

  get success(): boolean {
    //
    return !this[this.failureMessageName];
  }

  get failed(): boolean {
    //
    return !!this[this.failureMessageName];
  }

  getFailureMessage(): FailureMessage {
    //
    return this[this.failureMessageName];
  }

  setFailureMessage(failureMessage: FailureMessage): void {
    //
    this[this.failureMessageName] = failureMessage;
  }

  [key: string]: any;
}

export default QueryResponse;
