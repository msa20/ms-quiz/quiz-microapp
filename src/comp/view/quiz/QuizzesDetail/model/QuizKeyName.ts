
enum QuizKeyName {
  no = '문제 번호',
  quizCategory = '카테고리',
  quizDifficultyLevel = '난이도',
  text = '문제',
  quizItems = '문항',
  correctSeq = '정답',
}


export default QuizKeyName;
