import { makeObservable, observable } from 'mobx';
import { DomainEntity, IdName, NameValueList } from '~/comp/shared';
import { QuizCategory, QuizDifficultyLevel, QuizItem, QuizState } from './vo';


class Quiz extends DomainEntity {
  groupId: string;
  quizSheetId: string;

  writer: IdName | null = null;
  no: string;
  text: string;
  subText: string;
  quizCategory: QuizCategory | null;

  quizDifficultyLevel: QuizDifficultyLevel | null;
  quizState: QuizState;
  correctSeq: string;
  quizItems: QuizItem[] = [];

  // for ui
  editing?: boolean;
  checkedAnswer?: string;

  constructor(
    groupId: string,
    quizSheetId: string,
    no: string,
    text: string,
    subText: string,
    quizCategory: QuizCategory | null,
    quizDifficultyLevel: QuizDifficultyLevel | null,
    quizState: QuizState,
    correctSeq: string,
    quizItems: QuizItem[],
    editing?: boolean,
  ) {
    super();
    this.groupId = groupId;
    this.quizSheetId = quizSheetId;
    this.no = no;
    this.text = text;
    this.subText = subText;
    this.quizCategory = quizCategory;
    this.quizDifficultyLevel = quizDifficultyLevel;
    this.quizState = quizState;
    this.correctSeq = correctSeq;
    this.quizItems = quizItems;
    this.checkedAnswer = '';

    this.editing = editing;

    makeObservable(this, {
      groupId: observable,
      quizSheetId: observable,
      writer: observable,
      no: observable,
      text: observable,
      subText: observable,
      quizCategory: observable,
      quizDifficultyLevel: observable,
      quizState: observable,
      correctSeq: observable,
      quizItems: observable,

      checkedAnswer: observable,
      editing: observable,
    });
  }

  static fromDomain(domain: Quiz): Quiz {
    const quiz = new Quiz(
      domain.groupId,
      domain.quizSheetId,
      domain.no,
      domain.text,
      domain.subText,
      domain.quizCategory,
      domain.quizDifficultyLevel,
      domain.quizState,
      domain.correctSeq,
      domain.quizItems,
    );

    quiz.setDomainEntity(domain);
    quiz.writer = domain.writer;
    return quiz;
  }

  static fromDomains(domains: Quiz[]): Quiz[] {
    return domains.map(domain => this.fromDomain(domain));
  }

  static asNameValues(model: Quiz): NameValueList {
    return {
      nameValues: [
        {
          name: 'no',
          value: String(model.no),
        },
        {
          name: 'text',
          value: String(model.text),
        },
        {
          name: 'subText',
          value: String(model.subText),
        },
        {
          name: 'quizCategory',
          value: String(model.quizCategory),
        },
        {
          name: 'quizDifficultyLevel',
          value: String(model.quizDifficultyLevel),
        },
        {
          name: 'quizState',
          value: String(model.quizState),
        },
        {
          name: 'correctSeq',
          value: String(model.correctSeq),
        },
        {
          name: 'quizItems',
          value: JSON.stringify(model.quizItems),
        },
      ],
    };
  }

  static new(groupId: string, quizSheetId: string): Quiz {
    return new Quiz(groupId, quizSheetId, '', '', '', null, null, QuizState.Working, '', []);
  }

  static newDefault(groupId: string, quizSheetId: string, writer: IdName): Quiz {
    //
    const quiz = new Quiz(groupId, quizSheetId, '', '', '', null, null, QuizState.Working, '', [], true);

    quiz.writer = writer;
    quiz.quizItems = [QuizItem.new()];

    return quiz;
  }

}

export default Quiz;
