enum QuizGradingResult {
  Correct = 'Correct',
  InCorrect = 'InCorrect',
}

export default QuizGradingResult;
