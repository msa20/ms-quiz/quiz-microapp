enum QuizSheetState {
  Working = 'Working',
  Published = 'Published',
}

export default QuizSheetState;
