import { makeObservable, observable } from 'mobx';
import { DomainEntity, IdName, NameValueList } from '~/comp/shared';


class QuizAnswerSheet extends DomainEntity {
  groupId: string;
  quizSheetId: string;
  writer: IdName | null = null;
  subject: string;
  passed: Boolean;

  constructor(groupId: string, quizSheetId: string, subject: string, passed: Boolean) {
    super();
    this.groupId = groupId;
    this.quizSheetId = quizSheetId;
    this.subject = subject;
    this.passed = passed;

    makeObservable(this, {
      groupId: observable,
      quizSheetId: observable,
      writer: observable,
      subject: observable,
      passed: observable,
    });
  }

  static fromDomain(domain: QuizAnswerSheet): QuizAnswerSheet {
    const quizAnswerSheet = new QuizAnswerSheet(
      domain.groupId,
      domain.quizSheetId,
      domain.subject,
      domain.passed,
    );

    quizAnswerSheet.setDomainEntity(domain);
    quizAnswerSheet.writer = domain.writer;
    return quizAnswerSheet;
  }

  static fromDomains(domains: QuizAnswerSheet[]): QuizAnswerSheet[] {
    return domains.map(domain => this.fromDomain(domain));
  }

  static new(groupId: string, quizSheetId: string): QuizAnswerSheet {
    return new QuizAnswerSheet(groupId, quizSheetId, '', false);
  }

  static asNameValues(model: QuizAnswerSheet): NameValueList {
    return {
      nameValues: [
        {
          name: 'passed',
          value: String(model.passed),
        },
      ],
    };
  }

  get writerName(): string {
    return this.writer && this.writerName || '';
  }


}

export default QuizAnswerSheet;
