import { CqrsDynamicQuery } from '~/comp/shared';
import { QuizAnswer } from '../../api-model';


class QuizAnswersDynamicQuery extends CqrsDynamicQuery<QuizAnswer[]> {
}

export default QuizAnswersDynamicQuery;
