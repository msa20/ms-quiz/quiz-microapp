import { CqrsBaseQuery } from '~/comp/shared';
import { Quiz } from '../../api-model';


class QuizQuery extends CqrsBaseQuery<Quiz> {
  quizId: string;

  constructor(quizId: string) {
    super();
    this.quizId = quizId;
  }

  static by(quizId: string) {
    const query = new QuizQuery(quizId);
    return query;
  }

}

export default QuizQuery;
